#include "ros/ros.h"
#include "std_msgs/String.h"
#include "communication/Custom_msg.h"
#include <string>
#include <sstream>

std::string auv_id;

void callback(const  communication::Custom_msg::ConstPtr& msg){
   ROS_INFO("Time now: %f", ros::Time::now().toSec());
   ROS_INFO("[Modem %s]: I heard: [%s], delay: %f", auv_id.c_str(), msg->payload.c_str(), (ros::Time::now() - msg->time).toSec());
}

int main(int argc, char **argv)
{
   std::string starting{"Acoustic Modem n. "};
   std::string node_name{"acoustic_modem_"};

   if (argc > 1){
      auv_id = argv[1];
      starting.append(auv_id);
      ROS_INFO_STREAM(starting);
      node_name.append(auv_id);
      ros::init(argc, argv, node_name);
   }
   else{
      ROS_INFO("Acoustic Modem Anonymous");
      ros::init(argc, argv, node_name, ros::init_options::AnonymousName);
   }

   ros::NodeHandle nh("~");

   int rate;
   // Rate from parameter server
   try {
      if(!nh.getParam("/rate", rate)) throw std::string("Failed to read 'rate' from parameter server");
   }
   catch (const std::string &e) {
      ROS_ERROR_STREAM(e);
      ROS_INFO("Node shutdown...");
      return 0;
   }

   // From underwater channel: topic to receive messages from the underwater channel node
   std::string channel_rx{"from_uw_channel"};
   // To underwater channel: topic to transmit messages to the underwater channel node
   std::string channel_tx{"to_uw_channel"};

   // Publisher and Subscriber
   ros::Publisher pub = nh.advertise<communication::Custom_msg>(channel_tx, 1);
   ros::Subscriber sub = nh.subscribe(channel_rx, 1, callback);

   ros::Rate loop_rate(rate);

   srand((unsigned) time(0));
   ros::AsyncSpinner spinner(2);
   ros::Duration(2).sleep();


   while (ros::ok()){

      spinner.start();
      communication::Custom_msg msg;
      std::stringstream ss;
      ss << "Hello from Modem n." << auv_id;
      msg.payload = ss.str();
      msg.time = ros::Time::now();
      //ROS_INFO("%s", msg.data.c_str());
      pub.publish(msg);
      //ROS_INFO("Sent msg: %f", ros::Time::now().toSec());

      // Sending messages with two operating mode.

      // FIRST:
      // Note: This make asynchronous the broadcast of each acoustic modem
      // In this way, the average bandwidth tends to the same value for all the AUVs, but the transients are different
      //ros::Duration((rand() % 3)).sleep();

      // SECOND:
      // Sends every 4 seconds
      ros::Duration(4).sleep();
   }
   ros::waitForShutdown();
   return 0;
}