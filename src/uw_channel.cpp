#include <ros/ros.h>
#include <ros/time.h>
#include <vector>
#include <ctime>
#include <cmath>

#include <Eigen/Eigen>
#include <Eigen/Dense>

#include "communication/Custom_msg.h"

#define SOUND_VEL 1500 // Acoustic wave velocity in a water channel expressed in [m/s]

// Vector of Publishers and Subscribers
std::vector<ros::Subscriber> subs;
std::vector<ros::Publisher> pubs;
std::vector<ros::Timer> event;

using namespace Eigen;

MatrixXd connected;

// TODO: modello per calcolo velocità del suono
// Range of validity: temperature 0 to 40 °C, salinity 0 to 40 parts per thousand, pressure 0 to 1000 bar
// (Wong and Zhu, 1995) {https://asa.scitation.org/doi/abs/10.1121/1.413048}.

// Sigmoid function: f(500) = 0.98 ; f(1000) = 0.02
float sigmoid(float x){
   return 1.0/(1.0+exp(0.016*(x-750.0)));
}

// Randomly simulates the position of two points
void move_auv(std::vector<VectorXd> &auv_pos){
   for(auto &pos: auv_pos){
      pos(0) =  (rand() % 800) - 400;
      pos(1) =  (rand() % 800) - 400;
      pos(2) =  (rand() % 800) - 400;
   }
}

// Euclidean distance between two points
float dist_auv(const VectorXd &pos_1, const VectorXd &pos_2){
   return sqrt(pow(pos_1(0)-pos_2(0), 2) + pow(pos_1(1)-pos_2(1), 2) + pow(pos_1(2)-pos_2(2), 2));
}

// Trivially evaluate delay as the ratio between distance and constant SOUND_VEL, then adds the band_Delay
float delay(float dist, float band_delay){
   return dist/SOUND_VEL + band_delay;
}

// Checks distance among AUVs. Obtains the probability through the sigmoid and evaluate if the packet will arrive.
// Fills the matrix "connected" with the delay when the requirements are meet
void check_connection(MatrixXd &connected, const  std::vector<VectorXd> &auv_pos, float band_delay){
   int cols = connected.cols();
   for(int i = 0; i < cols; i++){
      for(int j = 0; j < cols; j++){
         if(j==i)
            continue;
         else{
            float dist = dist_auv(auv_pos[i], auv_pos[j]);
            float prob = sigmoid(dist);
            ROS_INFO("Prob: %f", prob);
            if((rand()%100 / 100.0) < prob)
               connected(i,j) = delay(dist, band_delay);
            else
               connected(i,j) = 0.0;
         }
      }
   }
}

void timer_callback(const ros::TimerEvent& event, const communication::Custom_msg::ConstPtr& msg, int to_who){
   // Debug
   //ROS_INFO("[timer_callback] Delay up to now: %f", (ros::Time::now() - msg->time).toSec());
   pubs[to_who].publish(msg);
}

void callback(const communication::Custom_msg::ConstPtr& msg, const ros::NodeHandle &nh, int auv_id){
   // Debug
   //ROS_INFO("[Channel] I heard: [%s], sent from auv [%d]", msg->payload.c_str(), auv_id);
   //ROS_INFO_STREAM("Size event: " << event.size());
   for(int i = 0; i < connected.cols(); i++){
      if(i == (auv_id-1))
         continue;
      else{
         float delay = connected(auv_id-1, i);
         //ROS_INFO("connected(%d,%d) = %f", auv_id-1, i, delay);
         if(delay > 0){
            //ROS_INFO("ENtrato!!!!!!!!!!! a [%f]", ros::Time::now().toSec());
            //ROS_INFO("[callback] Delay up to now: %f", (ros::Time::now() - msg->time).toSec());
            ros::Timer timer = nh.createTimer(ros::Duration(delay), boost::bind(timer_callback, _1, msg, i), true);
            event.emplace_back(timer);
         }
      }
   }
}

int main(int argc, char **argv){

   // Node initialization
   ROS_INFO("Starting Underwater Channel");
   ros::init(argc, argv, "underwater_channel_node");
   ros::NodeHandle nh("~");

   // Extracting values from parameter server
   int rate, auv_num, bandwidth, packet_dim;

   try {
      if(!nh.getParam("/rate", rate)) throw std::string("Failed to read 'rate' from parameter server");
      if(!nh.getParam("/auv_num", auv_num)) throw std::string("Failed to read 'auv_num' from parameter server");
      if(!nh.getParam("/bandwidth", bandwidth)) throw std::string("Failed to read 'bandwidth' from parameter server");
      if(!nh.getParam("/packet_dim", packet_dim)) throw std::string("Failed to read 'packet_dim' from parameter server");
   }
   catch (const std::string &e) {
      ROS_ERROR_STREAM(e);
      ROS_INFO("Node shutdown...");
      return 0;
   }

   // Delay introduced by the bandwidth
   float band_delay = packet_dim/bandwidth;

   // Initialize position of AUVs
   std::vector<VectorXd> auv_pos;
   for(int i=0; i< auv_num; ++i)
      auv_pos.emplace_back(VectorXd::Zero(6));

   // Initialize random with time, needed to simulate AUV's random movements
   srand((unsigned) time(0));

   // Auv's movement period
   ros::Duration mov_period(6, 0);

   // Matrix which defines the communication:
   // Each row define an auv, the cols the other ones.
   // The cell value represent the delay among auvs, with the only expection that the cell value is 0 if
   // the message from the j-th AUV can not reach the i-th one (i for rows and j for cols).
   connected = MatrixXd::Zero(auv_num, auv_num);

   ros::Rate loop_rate(rate);
   ros::Time t2;
   ros::Time t1 = ros::Time::now();
   ros::Time begin_sim = t1;

   // AUVs ID in a vector
   std::vector<int> auv_id;
   for(int i=1; i<=auv_num; ++i)
      auv_id.push_back(i);

   // Vector of Subscribers: assuming that each node which wants to communicate with the channel  is named
   // "acoustic_modem_ID" with ID a number starting from 1.
   for(int i=1; i<=auv_num; i++){
      std::string channel_rx{"/acoustic_modem_"};
      channel_rx.append(std::to_string(i));
      channel_rx.append("/to_uw_channel");
      ros::Subscriber sub;
      sub = nh.subscribe<communication::Custom_msg>(channel_rx, 1, boost::bind(callback, _1, nh, auv_id[i-1]) );
      subs.emplace_back(sub);
   }

   // Vector of Publishers: same as Subscribers.
   for(int i=1; i<=auv_num; i++){
      std::string channel_tx{"/acoustic_modem_"};
      channel_tx.append(std::to_string(i));
      channel_tx.append("/from_uw_channel");
      ros::Publisher pub = nh.advertise<communication::Custom_msg>(channel_tx, 1);
      pubs.emplace_back(pub);
   }

   // Use 4 threads to manage callbacks in parallel
   ros::AsyncSpinner spinner(4);

   while(ros::ok){

      spinner.start();
      //Debug
      t2 = ros::Time::now();
      if(t2 - t1 >= mov_period){
         move_auv(auv_pos);
         ROS_INFO("At time: [%f]", (t1-begin_sim).toSec());
         //ROS_INFO("AUV1 (x,y,z): [%.2f, %.2f, %.2f] ", auv_pos[0](0), auv_pos[0](1), auv_pos[0](2));
         //ROS_INFO("AUV2 (x,y,z): [%.2f, %.2f, %.2f] ", auv_pos[1](0), auv_pos[1](1), auv_pos[1](2));
         t1 = ros::Time::now();
         check_connection(connected, auv_pos, band_delay);
         //ROS_INFO("Distance AUV1/AUV2: [%f] ", dist_auv(auv_pos[0], auv_pos[1]));

         // Debug with 2 AUVs
         ROS_INFO("Delay AUV1/AUV2 (if 0.0 no trans): [%f]", connected(0,1));
         ROS_INFO("Delay AUV2/AUV1 (if 0.0 no trans): [%f]", connected(1,0));


         /*// Debug with 3 AUVs
         ROS_INFO("Delay AUV1/AUV2 (if 0.0 no trans): [%f]", connected(0,1));
         ROS_INFO("Delay AUV1/AUV3 (if 0.0 no trans): [%f]", connected(0,2));
         ROS_INFO("Delay AUV2/AUV1 (if 0.0 no trans): [%f]", connected(1,0));
         ROS_INFO("Delay AUV2/AUV3 (if 0.0 no trans): [%f]", connected(1,2));
         ROS_INFO("Delay AUV3/AUV1 (if 0.0 no trans): [%f]", connected(2,0));
         ROS_INFO("Delay AUV3/AUV2 (if 0.0 no trans): [%f]", connected(2,1));
          */

         /*// Debug with 4 AUVs
         ROS_INFO("Delay AUV1/AUV2 (if 0.0 no trans): [%f]", connected(0,1));
         ROS_INFO("Delay AUV1/AUV3 (if 0.0 no trans): [%f]", connected(0,2));
         ROS_INFO("Delay AUV1/AUV4 (if 0.0 no trans): [%f]", connected(0,3));
         ROS_INFO("Delay AUV2/AUV1 (if 0.0 no trans): [%f]", connected(1,0));
         ROS_INFO("Delay AUV2/AUV3 (if 0.0 no trans): [%f]", connected(1,2));
         ROS_INFO("Delay AUV2/AUV4 (if 0.0 no trans): [%f]", connected(1,3));
         ROS_INFO("Delay AUV3/AUV1 (if 0.0 no trans): [%f]", connected(2,0));
         ROS_INFO("Delay AUV3/AUV2 (if 0.0 no trans): [%f]", connected(2,1));
         ROS_INFO("Delay AUV3/AUV4 (if 0.0 no trans): [%f]", connected(2,3));
         ROS_INFO("Delay AUV4/AUV1 (if 0.0 no trans): [%f]", connected(2,0));
         ROS_INFO("Delay AUV4/AUV2 (if 0.0 no trans): [%f]", connected(2,1));
         ROS_INFO("Delay AUV4/AUV3 (if 0.0 no trans): [%f]", connected(2,2));
          */
      }
      loop_rate.sleep();
   }
   ros::waitForShutdown();
   return 0;
}
